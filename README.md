A sample demo project of a spring boot application integrated with keycloak. This uses the resource owner password flow with bearer-only.
There is also an example of using the admin client to create users.

Some helpful gists -
- https://gist.github.com/rkbalgi/07f39dd1ad45d402058613d28890bb90
- https://gist.github.com/rkbalgi/ae1169e047fa88460791434beaafeb74
- https://gist.github.com/rkbalgi/2b605c86a4d50def73f9aced5619396b

Thomas Darimont maintains loads of good stuff and sample projects dealing with keycloak -

- https://github.com/thomasdarimont/awesome-keycloak
- https://gist.github.com/thomasdarimont/c4e739c5a319cf78a4cff3b87173a84b